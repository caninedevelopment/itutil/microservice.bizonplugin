﻿namespace Monosoft.Service.Plugin.V1.Commands.Version
{
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    /// <summary>
    /// cmsPageFragment.
    /// </summary>
    public class Namespace : BaseNamespace
    {
        public static DBContext db = new DBContext();
        public Namespace() : base("Bizon_PluginVersion", new ProgramVersion("1.0.0.0"))
        {
            this.commands.Add(new create());
            this.commands.Add(new get());
            this.commands.Add(new update());
        }

        /// <summary>
        /// Gets Claims.
        /// </summary>
        internal static Claim Claims =>
                    new Claim()
                    {
                        key = "Monosoft.Service.Plugin.Administrator",
                        dataContext = Claim.DataContextEnum.organisationClaims,
                        description = new LocalizedString[]
                        {
                            new LocalizedString() { lang = "en", text = "plugin administrator" },
                        },
                    };

    }
}