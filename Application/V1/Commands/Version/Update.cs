﻿namespace Monosoft.Service.Plugin.V1.Commands.Version
{
    using ITUtil.Common.Command;

    /// <summary>
    /// createOrUpdate.
    /// </summary>
    public class update : InsertCommand<DTO.VersionInfo>
    {
        public update() : base("Update a plugin version")
        {
            this.claims.Add(Namespace.Claims);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.Fragment input.</param>
        public override void Execute(DTO.VersionInfo input)
        {
            Namespace.db.UpdateVersion(input);
        }
    }

}
