﻿namespace Monosoft.Service.Plugin.V1.Commands.Version
{
    using ITUtil.Common.Command;
    using ITUtil.Common.DTO;
    using Monosoft.Service.Plugin.V1.DTO;

    /// <summary>
    /// createOrUpdate.
    /// </summary>
    public class get : GetCommand<GuidIdDTO, VersionFile>
    {
        public get() : base("Get a version")
        {
            this.claims.Add(Namespace.Claims);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.Fragment input.</param>
        public override VersionFile Execute(GuidIdDTO input)
        {
            return Namespace.db.GetVersionFile(input.id);
        }
    }

}
