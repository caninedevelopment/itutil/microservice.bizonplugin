﻿namespace Monosoft.Service.Plugin.V1.Commands.Version
{
    using ITUtil.Common.Command;

    /// <summary>
    /// createOrUpdate.
    /// </summary>
    public class create : InsertCommand<DTO.VersionInfo>
    {
        public create() : base("Create a plugin version")
        {
            this.claims.Add(Namespace.Claims);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.Fragment input.</param>
        public override void Execute(DTO.VersionInfo input)
        {
            Namespace.db.CreateVersion(input);
        }
    }

}
