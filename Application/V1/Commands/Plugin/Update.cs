﻿namespace Monosoft.Service.Plugin.V1.Commands.Plugin
{
    using ITUtil.Common.Command;

    /// <summary>
    /// createOrUpdate.
    /// </summary>
    public class update : InsertCommand<DTO.PluginWithNoVersion>
    {
        public update() : base("Update a plugin")
        {
            this.claims.Add(Namespace.Claims);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.Fragment input.</param>
        public override void Execute(DTO.PluginWithNoVersion input)
        {
            Namespace.db.UpdatePlugin(input);
        }
    }

}
