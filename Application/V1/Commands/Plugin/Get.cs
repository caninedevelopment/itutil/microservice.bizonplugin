﻿namespace Monosoft.Service.Plugin.V1.Commands.Plugin
{
    using ITUtil.Common.Command;
    using ITUtil.Common.DTO;
    using Monosoft.Service.Plugin.V1.DTO;

    /// <summary>
    /// createOrUpdate.
    /// </summary>
    public class get : GetCommand<GuidIdDTO, PluginInfo>
    {
        public get() : base("Get a plugin")
        {
            this.claims.Add(Namespace.Claims);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.Fragment input.</param>
        public override PluginInfo Execute(GuidIdDTO input)
        {
            return Namespace.db.GetPlugin(input.id);
        }
    }

}
