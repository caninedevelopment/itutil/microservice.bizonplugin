﻿namespace Monosoft.Service.Plugin.V1.Commands.Plugin
{
    using ITUtil.Common.Command;

    /// <summary>
    /// createOrUpdate.
    /// </summary>
    public class create : InsertCommand<DTO.PluginWithNoVersion>
    {
        public create() : base("Create a plugin")
        {
            this.claims.Add(Namespace.Claims);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.Fragment input.</param>
        public override void Execute(DTO.PluginWithNoVersion input)
        {
            Namespace.db.CreatePlugin(input);
        }
    }

}
