﻿namespace Monosoft.Service.Plugin.V1.Commands.Plugin
{
    using ITUtil.Common.Command;
    using ITUtil.Common.DTO;
    using Monosoft.Service.Plugin.V1.DTO;

    /// <summary>
    /// createOrUpdate.
    /// </summary>
    public class GetAll : GetCommand<object, PluginInfoList>
    {
        public GetAll() : base("Get all plugins")
        {
            this.claims.Add(Namespace.Claims);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.Fragment input.</param>
        public override PluginInfoList Execute(object input)
        {
            return new PluginInfoList() { Plugins = Namespace.db.GetAll() };
        }
    }

}
