﻿namespace Monosoft.Service.Plugin.V1.Commands.Plugin
{
    using ITUtil.Common.Command;
    using ITUtil.Common.DTO;

    /// <summary>
    /// createOrUpdate.
    /// </summary>
    public class delete : InsertCommand<GuidIdDTO>
    {
        public delete() : base("Create a plugin")
        {
            this.claims.Add(Namespace.Claims);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.Fragment input.</param>
        public override void Execute(GuidIdDTO input)
        {
            Namespace.db.Remove(input.id);
        }
    }

}
