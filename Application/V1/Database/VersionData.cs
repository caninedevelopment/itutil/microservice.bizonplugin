﻿// <copyright file="VersionData.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Plugin.V1.Database
{
    using ServiceStack.DataAnnotations;
    using System;

    /// <summary>
    /// VersionData.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class VersionData
    {
        /// <summary> 
        /// Gets or sets id.
        /// </summary>
        public Guid id { get; set; }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets microservice.
        /// </summary>
        public string microservice { get; set; }

        /// <summary>
        /// Gets or sets dependencies.
        /// </summary>
        public string dependencies { get; set; }

        /// <summary>
        /// Gets or sets releaseNote.
        /// </summary>
        public string releaseNote { get; set; }

        /// <summary>
        /// Gets or sets releaseDate.
        /// </summary>
        public string releaseDate { get; set; }

        /// <summary>
        /// Gets or sets pluginId.
        /// </summary>
        public Guid pluginId { get; set; }
    }
}
