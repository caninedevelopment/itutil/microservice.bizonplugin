// <copyright file="DatabaseLogic.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Plugin.V1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using Monosoft.Service.Plugin.V1.DTO;
    using ServiceStack.OrmLite;

    /// <summary>
    /// Database.
    /// </summary>
    public class DBContext
    {
        private static string dbName = "plugin";
        private static OrmLiteConnectionFactory dbFactory = null;

        private static OrmLiteConnectionFactory GetConnectionFactory(SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLSettings.SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServer2016Dialect.Provider);
                case SQLSettings.SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLSettings.SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DBContext"/> class.
        /// </summary>
        /// <param name="settingsFilename">string dbName.</param>
        public DBContext(string settingsFilename = null)
        {
            var settings = MicroServiceConfig.getConfig(settingsFilename);
            OrmLiteConnectionFactory masterDb = null;
            masterDb = GetConnectionFactory(settings.SQL);

            using (var db = masterDb.Open())
            {
                settings.SQL.CreateDatabaseIfNoExists(db, dbName);
            }

            dbFactory = GetConnectionFactory(settings.SQL, dbName);
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<Plugin.V1.Database.PluginData>();
                db.CreateTableIfNotExists<Plugin.V1.Database.VersionData>();
                db.CreateTableIfNotExists<Plugin.V1.Database.VersionFileData>();
            }
        }

        /// <summary>
        /// Create.
        /// </summary>
        /// <param name="input">PluginInfo input.</param>
        public void CreatePlugin(PluginWithNoVersion input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Plugin.V1.Database.PluginData>(input.id);

                if (dbresult == null)
                {
                    Plugin.V1.Database.PluginData plugin = new Plugin.V1.Database.PluginData();
                    plugin.id = input.id;
                    plugin.name = input.name;
                    plugin.description = input.description;
                    plugin.imageDataUri = input.imageDataUri;
                    plugin.independent = input.independent;
                    db.Insert<Plugin.V1.Database.PluginData>(plugin);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementAlreadyExistsException("Plugin already exists", input.id.ToString());
                }
            }
        }

        /// <summary>
        /// Update.
        /// </summary>
        /// <param name="input">PluginInfo input.</param>
        public void UpdatePlugin(PluginWithNoVersion input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Plugin.V1.Database.PluginData>(input.id);

                if (dbresult != null)
                {
                    Plugin.V1.Database.PluginData plugin = new Plugin.V1.Database.PluginData();
                    plugin.id = input.id;
                    plugin.name = input.name;
                    plugin.description = input.description;
                    plugin.imageDataUri = input.imageDataUri;
                    plugin.independent = input.independent;
                    db.Update<Plugin.V1.Database.PluginData>(input);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Plugin does not exist", input.id.ToString());
                }
            }
        }

        /// <summary>
        /// Create.
        /// </summary>
        /// <param name="input">PluginInfo input.</param>
        public void CreateVersion(VersionInfo input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Plugin.V1.Database.VersionData>(input.id);

                if (dbresult == null)
                {
                    Plugin.V1.Database.VersionFileData file = new Plugin.V1.Database.VersionFileData();
                    file.versionId = input.id;
                    file.versionFileBase64 = input.versionFileBase64;

                    Plugin.V1.Database.VersionData version = new Plugin.V1.Database.VersionData();
                    version.id = input.id;
                    version.name = input.name;
                    version.microservice = input.microservice;
                    version.dependencies = input.dependencies;
                    version.releaseNote = input.releaseNote;
                    version.releaseDate = input.releaseDate;
                    version.pluginId = input.pluginId;

                    db.Insert<Plugin.V1.Database.VersionData>(version);
                    db.Insert<Plugin.V1.Database.VersionFileData>(file);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementAlreadyExistsException("Version already exist", input.id.ToString());
                }
            }
        }

        /// <summary>
        /// Update.
        /// </summary>
        /// <param name="input">PluginInfo input.</param>
        public void UpdateVersion(VersionInfo input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Plugin.V1.Database.VersionData>(input.id);

                if (dbresult != null)
                {
                    Plugin.V1.Database.VersionFileData file = new Plugin.V1.Database.VersionFileData();
                    file.versionId = input.id;
                    file.versionFileBase64 = input.versionFileBase64;

                    Plugin.V1.Database.VersionData version = new Plugin.V1.Database.VersionData();
                    version.id = input.id;
                    version.name = input.name;
                    version.microservice = input.microservice;
                    version.dependencies = input.dependencies;
                    version.releaseNote = input.releaseNote;
                    version.releaseDate = input.releaseDate;
                    version.pluginId = input.pluginId;

                    db.Update<Plugin.V1.Database.VersionFileData>(file);
                    db.Update<Plugin.V1.Database.VersionData>(version);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Version does not exist", input.id.ToString());
                }
            }
        }

        /// <summary>
        /// Remove.
        /// </summary>
        /// <param name="id">Guid id.</param>
        public void Remove(Guid id)
        {
            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Plugin.V1.Database.PluginData>(id);

                if (dbresult != null)
                {
                    db.DeleteById<Plugin.V1.Database.PluginData>(id);
                    db.DeleteById<Plugin.V1.Database.VersionFileData>(id);
                    db.Delete<Plugin.V1.Database.VersionData>(x => x.pluginId == id);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Plugin does not exist", id.ToString());
                }
            }
        }

        /// <summary>
        /// GetPlugin.
        /// </summary>
        /// <param name="id">Guid id.</param>
        /// <returns>PluginInfo.</returns>
        public PluginInfo GetPlugin(Guid id)
        {
            PluginInfo plugin = new PluginInfo();
            List<VesionWithNoFile> listversion = new List<VesionWithNoFile>();
            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Plugin.V1.Database.PluginData>(id);
                if (dbresult != null)
                {
                    var res = db.Select<Plugin.V1.Database.VersionData>(x => x.pluginId == id);
                    if (res != null)
                    {
                        plugin.id = dbresult.id;
                        plugin.name = dbresult.name;
                        plugin.description = dbresult.description;
                        plugin.imageDataUri = dbresult.imageDataUri;
                        plugin.independent = dbresult.independent;
                        plugin.vesionWithNoFile = new List<VesionWithNoFile>();

                        foreach (var version in res)
                        {
                            plugin.vesionWithNoFile.Add(new VesionWithNoFile()
                            {
                                id = version.id,
                                releaseDate = version.releaseDate,
                                releaseNote = version.releaseNote,
                                dependencies = version.dependencies,
                                microservice = version.microservice,
                                name = version.name,
                                pluginId = version.pluginId,
                            });
                        }
                    }
                } else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Plugin does not exist", id.ToString());
                }

                return plugin;
            }
        }

        /// <summary>
        /// GetPlugin.
        /// </summary>
        /// <param name="id">Guid id.</param>
        /// <returns>PluginInfo.</returns>
        public VersionFile GetVersionFile(Guid id)
        {
            VersionFile versionfile = new VersionFile();

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Plugin.V1.Database.VersionFileData>(id);
                if (dbresult != null)
                {
                    versionfile.versionFileBase64 = dbresult.versionFileBase64;
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Version does not exist", id.ToString());
                }

                return versionfile;
            }
        }

        /// <summary>
        /// GetAll.
        /// </summary>
        /// <returns>List PluginInfo.</returns>
        public List<PluginInfo> GetAll()
        {
            List<PluginInfo> retunelist = new List<PluginInfo>();

            using (var db = dbFactory.Open())
            {
                var allPlugins = db.Select<Plugin.V1.Database.PluginData>();
                var allVersions = db.Select<Plugin.V1.Database.VersionData>();

                foreach (var plugin in allPlugins)
                {
                    var pluginInfo = new PluginInfo()
                    {
                        description = plugin.description,
                        id = plugin.id,
                        imageDataUri = plugin.imageDataUri,
                        independent = plugin.independent,
                        name = plugin.name,
                        vesionWithNoFile = new List<VesionWithNoFile>(),
                    };

                    foreach (var version in allVersions.Where(p => p.pluginId == plugin.id))
                    {
                        pluginInfo.vesionWithNoFile.Add(new VesionWithNoFile()
                        {
                            id = version.id,
                            releaseDate = version.releaseDate,
                            releaseNote = version.releaseNote,
                            dependencies = version.dependencies,
                            microservice = version.microservice,
                            name = version.name,
                            pluginId = version.pluginId,
                        });
                    }

                    retunelist.Add(pluginInfo);
                }

                return retunelist;
            }
        }
    }
}
