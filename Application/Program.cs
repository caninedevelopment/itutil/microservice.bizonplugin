﻿
namespace Monosoft.Service.Plugin
{
    class Program
    {
        static void Main(string[] args)
        {
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                new Monosoft.Service.Plugin.V1.Commands.Plugin.Namespace(),
                new Monosoft.Service.Plugin.V1.Commands.Version.Namespace(),
            }
            );
        }
    }
}
