﻿namespace Unittests
{
    using System;
    using ITUtil.Common.DTO;

    public class Common
    {
        public static string[] configurations = { "appsettings.mssql.json", "appsettings.postgresql.json" };

        public static ITUtil.Common.DTO.MessageWrapper GetMockWrapper()
        {
            return new ITUtil.Common.DTO.MessageWrapper()
            {
                callerIp = "0.0.0.0",
                clientId = "NA",
                isDirectLink = false,
                issuedDate = DateTime.Now,
                messageId = "NA",
                tracing = new Tracing() { traceLevel = Tracing.Level.none },
                version = new ProgramVersion("1.0.0.0."),
            };
        }
    }
}