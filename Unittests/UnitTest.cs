// <copyright file="UnitTest.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.Utils;
    using Monosoft.Service.Plugin.V1;
    using NUnit.Framework;

    /// <summary>
    /// UnitTests.
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// Creat plugin.
        /// </summary>
        [Test]
        public void CreatPlugin()
        {
            foreach (var configfile in Common.configurations)
            {
                Monosoft.Service.Plugin.V1.Commands.Plugin.Namespace.db = new DBContext(configfile);
                Monosoft.Service.Plugin.V1.Commands.Plugin.create create = new Monosoft.Service.Plugin.V1.Commands.Plugin.create();
                create.Execute(
                    new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
                    {
                        id = Guid.NewGuid(),
                        name = "unittest",
                        description = "creat plugin unittest",
                        imageDataUri = "image uri",
                        independent = true,
                    });
            }
        }

        /// <summary>
        /// Creat Exists Plugin.
        /// </summary>
        [Test]
        public void CreatExistsplugin()
        {
            foreach (var configfile in Common.configurations)
            {
                Monosoft.Service.Plugin.V1.Commands.Plugin.Namespace.db = new DBContext(configfile);
                Monosoft.Service.Plugin.V1.Commands.Plugin.create create = new Monosoft.Service.Plugin.V1.Commands.Plugin.create();
                Assert.Throws<ITUtil.Common.Base.ElementAlreadyExistsException>(() =>
                {
                    create.Execute(
                        new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
                        {
                            id = Guid.NewGuid(),
                            name = "unittest",
                            description = "creat plugin unittest",
                            imageDataUri = "image uri",
                            independent = true,
                        });
                }
                );
            }
        }


        //TODO:::
        ///// <summary>
        ///// Updata plugin.
        ///// </summary>
        //[Test]
        //public void Updataplugin()
        //{
        //    foreach (var configfile in Common.configurations)
        //    {
        //        Controller.db = new DBContext(configfile);
        //        var wrapper = Common.GetMockWrapper();
        //        var id = Guid.NewGuid();
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
        //            {
        //                id = id,
        //                name = "unittest",
        //                description = "creat plugin unittest",
        //                imageDataUri = "image uri",
        //                independent = true,
        //            });
        //        Controller.CreatePlugin(wrapper);

        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
        //            {
        //                id = id,
        //                name = "update",
        //                description = "update plugin unittest",
        //                imageDataUri = "image uri",
        //                independent = true,
        //            });

        //        var res = Controller.UpdatePlugin(wrapper);
        //        Assert.IsTrue(res.success);
        //    }
        //}

        ///// <summary>
        ///// Creat Version.
        ///// </summary>
        //[Test]
        //public void CreatVersion()
        //{
        //    foreach (var configfile in Common.configurations)
        //    {
        //        Controller.db = new DBContext(configfile);
        //        var wrapper = Common.GetMockWrapper();
        //        var id = Guid.NewGuid();
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
        //            {
        //                id = id,
        //                name = "unittest",
        //                description = "creat plugin unittest",
        //                imageDataUri = "image uri",
        //                independent = true,
        //            });
        //        Controller.CreatePlugin(wrapper);
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.VersionInfo>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.VersionInfo()
        //            {
        //                id = Guid.NewGuid(),
        //                name = "unittest",
        //                microservice = "this is a json string 2",
        //                dependencies = "this is a other json string 2",
        //                versionFileBase64 = "this is the path to where the file is 2",
        //                releaseNote = "this is the release note with hotfix",
        //                releaseDate = "this is the release date 2",
        //                pluginId = id,
        //            });
        //        var res = Controller.CreateVersion(wrapper);
        //        Assert.IsTrue(res.success);
        //    }
        //}

        ///// <summary>
        ///// Creat Exists Version.
        ///// </summary>
        //[Test]
        //public void CreatExistsVersion()
        //{
        //    foreach (var configfile in Common.configurations)
        //    {
        //        Controller.db = new DBContext(configfile);
        //        var wrapper = Common.GetMockWrapper();

        //        var id = Guid.NewGuid();
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
        //            {
        //                id = id,
        //                name = "unittest",
        //                description = "creat plugin unittest",
        //                imageDataUri = "image uri",
        //                independent = true,
        //            });
        //        Controller.CreatePlugin(wrapper);
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.VersionInfo>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.VersionInfo()
        //            {
        //                id = Guid.NewGuid(),
        //                name = "unittest",
        //                microservice = "this is a json string",
        //                dependencies = "this is a other json string",
        //                versionFileBase64 = "this is the path to where the file is",
        //                releaseNote = "this is the release note with hotfix",
        //                releaseDate = "this is the release date",
        //                pluginId = id,
        //            });
        //        Controller.CreateVersion(wrapper);
        //        var res = Controller.CreateVersion(wrapper);
        //        Assert.IsFalse(res.success);
        //    }
        //}

        ///// <summary>
        ///// Update Version.
        ///// </summary>
        //[Test]
        //public void UpdateVersion()
        //{
        //    foreach (var configfile in Common.configurations)
        //    {
        //        Controller.db = new DBContext(configfile);
        //        var wrapper = Common.GetMockWrapper();
        //        var versionid = Guid.NewGuid();
        //        var pluginid = Guid.NewGuid();
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
        //            {
        //                id = pluginid,
        //                name = "unittest",
        //                description = "creat plugin unittest",
        //                imageDataUri = "image uri",
        //                independent = true,
        //            });
        //        Controller.CreatePlugin(wrapper);
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.VersionInfo>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.VersionInfo()
        //            {
        //                id = versionid,
        //                name = "unittest",
        //                microservice = "this is a json string 2",
        //                dependencies = "this is a other json string 2",
        //                versionFileBase64 = "this is the path to where the file is 2",
        //                releaseNote = "this is the release note with hotfix",
        //                releaseDate = "this is the release date 2",
        //                pluginId = pluginid,
        //            });
        //        Controller.CreateVersion(wrapper);
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.VersionInfo>.SetData(
        //        wrapper,
        //        new Monosoft.Service.Plugin.V1.DTO.VersionInfo()
        //        {
        //            id = versionid,
        //            name = "update",
        //            microservice = "this is a json string 2",
        //            dependencies = "this is a other json string 2",
        //            versionFileBase64 = "this is the path to where the file is 2",
        //            releaseNote = "this is the release note with hotfix",
        //            releaseDate = "this is the release date 2",
        //            pluginId = pluginid,
        //        });
        //        var res = Controller.UpdateVersion(wrapper);
        //        Assert.IsTrue(res.success);
        //    }
        //}

        ///// <summary>
        ///// delete plugin.
        ///// </summary>
        //[Test]
        //public void DeletePlugin()
        //{
        //    foreach (var configfile in Common.configurations)
        //    {
        //        Controller.db = new DBContext(configfile);
        //        var wrapper = Common.GetMockWrapper();
        //        var id = Guid.NewGuid();
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
        //            {
        //                id = id,
        //                name = "unittest",
        //                description = "creat plugin unittest",
        //                imageDataUri = "image uri",
        //                independent = true,
        //            });
        //        Controller.CreatePlugin(wrapper);
        //        ITUtil.Common.DTO.MessageWrapperHelper<ITUtil.Common.DTO.GuidIdDTO>.SetData(
        //            wrapper,
        //            new ITUtil.Common.DTO.GuidIdDTO()
        //            {
        //                id = id,
        //            });
        //        var res = Controller.DeletePlugin(wrapper);
        //        Assert.IsTrue(res.success);
        //    }
        //}

        ///// <summary>
        ///// get plugin.
        ///// </summary>
        //[Test]
        //public void GetPlugin()
        //{
        //    foreach (var configfile in Common.configurations)
        //    {
        //        Controller.db = new DBContext(configfile);
        //        var wrapper = Common.GetMockWrapper();
        //        var id = Guid.NewGuid();
        //        var name = "read unittest";
        //        var des = "read plugin unittest";
        //        var img = "read image uri";
        //        var ind = true;
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
        //            {
        //                id = id,
        //                name = name,
        //                description = des,
        //                imageDataUri = img,
        //                independent = ind,
        //            });
        //        Controller.CreatePlugin(wrapper);
        //        ITUtil.Common.DTO.MessageWrapperHelper<ITUtil.Common.DTO.GuidIdDTO>.SetData(
        //            wrapper,
        //            new ITUtil.Common.DTO.GuidIdDTO()
        //            {
        //                id = id,
        //            });
        //        var res = Controller.GetPlugin(wrapper);
        //        var input = MessageDataHelper.FromMessageData<Monosoft.Service.Plugin.V1.DTO.PluginInfo>(res.data);
        //        Assert.AreEqual(input.id, id);
        //        Assert.AreEqual(input.name, name);
        //        Assert.AreEqual(input.description, des);
        //        Assert.AreEqual(input.imageDataUri, img);
        //        Assert.AreEqual(input.independent, ind);
        //    }
        //}

        ///// <summary>
        ///// get all plugin.
        ///// </summary>
        //[Test]
        //public void GetAllPlugin()
        //{
        //    foreach (var configfile in Common.configurations)
        //    {
        //        Controller.db = new DBContext(configfile);
        //        var wrapper = Common.GetMockWrapper();

        //        var beforres = Controller.GetAllePlugins(wrapper);
        //        var beforinput = MessageDataHelper.FromMessageData<List<Monosoft.Service.Plugin.V1.DTO.PluginInfo>>(beforres.data);
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion>.SetData(
        //            wrapper,
        //            new Monosoft.Service.Plugin.V1.DTO.PluginWithNoVersion()
        //            {
        //                id = Guid.NewGuid(),
        //                name = "read unittest",
        //                description = "read plugin unittest",
        //                imageDataUri = "read image uri",
        //                independent = true,
        //            });
        //        Controller.CreatePlugin(wrapper);
        //        wrapper = Common.GetMockWrapper();
        //        var res = Controller.GetAllePlugins(wrapper);
        //        var input = MessageDataHelper.FromMessageData<List<Monosoft.Service.Plugin.V1.DTO.PluginInfo>>(res.data);
        //        Assert.IsTrue(input.Count > beforinput.Count);
        //        Assert.IsTrue(input.Count == beforinput.Count + 1);
        //    }
        //}
    }
}